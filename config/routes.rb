EGo::Application.routes.draw do
  root to: 'pages#home'

  get '/admin', to: redirect('http://www.autobill.at')
  get '/admin/*x', to: redirect('http://www.autobill.at')
end
