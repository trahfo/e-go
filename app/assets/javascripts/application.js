// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require respond
//= require jquery
//= require jquery-ui
//= require jquery-easing
//= require jquery-magnific-popup
//= require jquery-scrollTo
//= require jquery-localscroll
//= require jquery-stellar
//= require jquery-flexslider
//= require jquery-isotope
//= require jquery_ujs
//= require modernizr
//= require bootstrap
//= require SmoothScroll
//= require custom

$(document).ready(function() {
  $('#flexHome').flexslider({
    animation: "slide",
    controlNav:true,
    directionNav:false,
    touch: true,
    direction: "vertical"
  });
});
