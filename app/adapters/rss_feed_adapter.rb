require 'open-uri'

class RssFeedAdapter
  URL = 'http://lebenimdorf.at/category/e-go/feed'
  YAHOO_MEDIA = {'media' => 'http://search.yahoo.com/mrss/'}

  def self.load_news(max = 3)
    doc = Nokogiri::XML(open(URL))
    items = []
    doc.xpath('//item').each do |item|
      items << {
        'title' => item.xpath('title').first.content,
        'date' => Date.parse(item.xpath('pubDate').first.content),
        'text' => item.xpath('description').first.content,
        'image' => item.xpath('media:thumbnail', YAHOO_MEDIA).first.attributes['url'].value,
        'link' => item.xpath('link').first.content
      }
    end
    items.first(max)
  end
end
