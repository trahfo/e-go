require 'open-uri'

class AutobillAdapter
  URL = 'http://www.autobill.at/stats/3.json'

  def self.load_stats
    JSON.parse(open(URL).read).symbolize_keys
  rescue
    {}
  end
end
