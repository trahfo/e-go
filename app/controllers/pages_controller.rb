class PagesController < ApplicationController
  def home
    @latest_news = RssFeedAdapter.load_news

    stats = AutobillAdapter.load_stats
    @users_count = stats[:users_count] || 0
    @total_km = (stats[:drives_sum_km] || 0).to_i + 1016
    @total_drives = stats[:drives_count] || 0
  end
end
