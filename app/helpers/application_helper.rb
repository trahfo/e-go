module ApplicationHelper

  def euro(x)
    number_to_currency(x)
  end

  def km(x)
    "#{x} km"
  end

  def min(x)
    "#{x} min"
  end

end
